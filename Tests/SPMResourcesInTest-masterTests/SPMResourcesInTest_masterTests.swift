import XCTest
@testable import SPMResourcesInTest_master

final class SPMResourcesInTest_masterTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SPMResourcesInTest_master().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
