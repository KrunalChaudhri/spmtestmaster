import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(SPMResourcesInTest_masterTests.allTests),
    ]
}
#endif
