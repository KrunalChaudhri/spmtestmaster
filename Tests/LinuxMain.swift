import XCTest

import SPMResourcesInTest_masterTests

var tests = [XCTestCaseEntry]()
tests += SPMResourcesInTest_masterTests.allTests()
XCTMain(tests)
